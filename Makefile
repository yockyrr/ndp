CFLAGS=-O2 -g -Wall -Wextra
ndp: lex.yy.c ndp-parser.c ndp-funcs.o
	cc $(CFLAGS) -o ndp ndp-funcs.o ndp-parser.c lex.yy.c -lfl
ndp-parser.c: ndp.y ndp-funcs.h
	yacc -vd -o ndp-parser.c ndp.y
lex.yy.c: ndp.l ndp-funcs.h
	lex ndp.l
ndp-funcs.o: ndp-funcs.c ndp-funcs.h ily-xxd.c usage-xxd.c licence-xxd.c format-xxd.c ndp-parser.c
	cc $(CFLAGS) -c -o ndp-funcs.o ndp-funcs.c
ily-xxd.c: ly/beginning.ily ly/layout.ily
	xxd -i ly/beginning.ily > ily-beginning-xxd.c
	xxd -i ly/layout.ily > ily-layout-xxd.c
usage-xxd.c: usage.txt
	xxd -i usage.txt > usage-xxd.c
format-xxd.c: format.txt
	xxd -i format.txt > format-xxd.c
licence-xxd.c: LICENCE.txt
	xxd -i LICENCE.txt > licence-xxd.c
clean:
	rm -f lex.yy.c ndp-funcs.o ndp ndp-parser.c ndp-parser.h ndp-parser.output ndp.tab.c ily-beginning-xxd.c ily-layout-xxd.c usage-xxd.c licence-xxd.c format-xxd.c
