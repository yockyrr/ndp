#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "ndp-parser.h"
#define ndp_malloc(c) __ndp_malloc(c, __FILE__, __LINE__)
#define ndp_realloc(p, c) __ndp_realloc(p, c, __FILE__, __LINE__)
#define ndp_warning(yylloc, fmt, ...) __ndp_message(yylloc, "Warning", __FILE__, __LINE__, fmt, ## __VA_ARGS__)
#if YYDEBUG == 0
# define ndp_debug(yylloc, fmt, ...) ;
#else
# define ndp_debug(yylloc, fmt, ...) __ndp_message(yylloc, "Debug", __FILE__, __LINE__, fmt, ## __VA_ARGS__)
#endif
#define ndp_error(yylloc, fmt, ...) {\
		__ndp_message(yylloc, "Error", __FILE__, __LINE__, fmt, ## __VA_ARGS__);\
		exit(1);\
}
#define ALPHANUM "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
#define SHARPSTR "is"
#define FLATSTR "es"
#define UP8STR "'"
#define DOWN8STR ","
#define OLIGSTR "\\[ "
#define CLIGSTR "\\] "
#define OCURSTR "\\( "
#define CCURSTR "\\) "
#define DIVMAJSTR "\\dI "
#define DIVMAXSTR "\\dII "
#define KEYCHANGESTR "\\setkey #"
#define CLEFCHANGESTR "\\setclef #"
#define PLICASTR "\\plica "
#define ADDURLSTR "\\setl "
#define NOSRC_BEGIN "\n\\override Score.TextSpanner.bound-details.left.text = \\markup \\teeny \"Not in "
#define ONOSRC "\"\n\\textLengthOn<>\\stopTextSpan\\startTextSpan"
#define CNOSRC "\n\\stopTextSpan"
#define EMPTYDIVSTR "\\once \\omit Dots \\once \\omit NoteHead \\once \\override NoteHead.no-ledgers = ##t \\once \\omit Stem \\once \\omit Accidental \\once \\omit Rest \\once \\omit TabNoteHead a4 "
FILE *outfile;
void set_outfile(FILE *stream);
void init_variables();
void *__ndp_malloc(size_t c, char *file, int line);
void *__ndp_realloc(void *ptr, size_t size, char *file, int line);
void __ndp_message(YYLTYPE *yylloc, char *type, char *file, int line, char *fmt, ...);

typedef enum ligaturetype {
	LIGATURE, CURRENTES, VIRGA, LIGCUR, DIVMAJ, DIVMAX, KEYCHANGE, CLEFCHANGE
} ligaturetype_t;
typedef enum altmustype {
	MUS, ALTLIST, NOSRC
} altmustype_t;

typedef struct note {
	int notename;
	int accidental;
	int octave;
	bool plica;
	YYLTYPE yylloc;
} *note_t;
typedef struct notelist {
	note_t note;
	struct notelist *next;
} *notelist_t;
typedef struct ligature {
	ligaturetype_t type;
	notelist_t *notes;
	size_t noteslen;
} *ligature_t;
typedef struct music {
	ligature_t ligature;
	struct music *next;
} *music_t;
typedef struct source {
	char *name;
	char *id;
	bool toggled;
	size_t diffnotes;
	size_t diffligs;
} *source_t;
typedef struct sourcelist {
	source_t source;
	struct sourcelist *next;
} *sourcelist_t;
typedef struct alt {
	sourcelist_t src;
	music_t mus;
} *alt_t;
typedef struct altlist {
	alt_t alt;
	struct altlist *next;
} *altlist_t;
typedef struct altmus {
	altmustype_t type;
	void *el;
	size_t idx;
	struct altmus *next;
} *altmus_t;
typedef struct voice {
	altmus_t altmus;
	struct voice *next;
} *voice_t;
typedef struct voicelist {
	voice_t voice;
	struct voicelist *next;
} *voicelist_t;
typedef struct ordo {
	voicelist_t voicelist;
	struct ordo *next;
} *ordo_t;
typedef struct syllable {
	char *str;
	int len;
} *syllable_t;
typedef struct lyriclist {
	syllable_t syllable;
	struct lyriclist *next;
} *lyriclist_t;

syllable_t make_syllable(char *str, int len);
lyriclist_t make_lyriclist(syllable_t syl, lyriclist_t ll);
note_t make_note(int nn, int acc, int oct, bool plica, YYLTYPE yylloc);
notelist_t make_notelist(note_t n, notelist_t c);
ligature_t make_ligature(notelist_t c);
ligature_t make_currentes(notelist_t c);
ligature_t make_ligcur(notelist_t a, notelist_t b);
ligature_t make_virga(note_t n);
ligature_t make_divisio_maior();
ligature_t make_divisio_maxima();
ligature_t make_keychange(int sharps, int curvidx);
ligature_t make_clefchange(int line, int curvidx);
music_t make_music(ligature_t l, music_t m);
alt_t make_alt(sourcelist_t s, music_t mus);
altlist_t make_altlist(alt_t a, altlist_t al, YYLTYPE yylloc);
altmus_t make_altmus_from_mus(music_t m, size_t curvidx);
altmus_t make_altmus_from_alt(altlist_t al, size_t curvidx);
voice_t make_voice(altmus_t a, voice_t c);
voicelist_t make_voicelist(voice_t v, voicelist_t vl);
voicelist_t make_nosrc(sourcelist_t s);
ordo_t make_ordo(voicelist_t a, ordo_t c);
source_t make_source(char *name, char *id);
source_t find_source(char *id);
sourcelist_t make_sourcelist(source_t s, sourcelist_t sl);
void make_definition(char *id, char *name);

void emit_file(ordo_t o, lyriclist_t lyr);
void emit_sourcelist_ids(sourcelist_t s, FILE *fp);
void emit_sourcelist_names(sourcelist_t s, FILE *fp);
void set_title(char *t);
void set_poet(char *p);
void set_composer(char *c);
void output_stats();

sourcelist_t ndp_sourcelist;
size_t maxv, curv, maxalt, curalt, curvidx;
char *title, *poet, *composer, *fullpath;
bool pointandclick;
