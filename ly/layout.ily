\layout {
  \context {
    \Score
    \consists "Text_spanner_engraver"
    \override SpacingSpanner.packed-spacing = ##t
    \remove "Default_bar_line_engraver"
  }
  \context {
    \Staff
    \remove "Time_signature_engraver"
    \remove "Default_bar_line_engraver"
  }
  \context {
    \Voice
    \remove "Forbid_line_break_engraver"
    \remove "Text_spanner_engraver"
  }
}
