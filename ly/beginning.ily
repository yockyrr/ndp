\version "2.21.0"
\include "gregorian.ly"
#(set-global-staff-size 14)
\pointAndClickOff
\paper {
  ragged-last-bottom = ##f
  ragged-last = ##f
  ragged-bottom = ##f
  ragged-right = ##f
  print-all-headers = ##t
  #(define fonts
    (set-global-fonts
      #:music "gonville"
      #:brace "gonville"
      #:roman "TeX Gyre Pagella"
      #:sans "Latin Modern Sans"
      #:typewriter "Latin Modern Mono"
      #:factor (/ staff-height pt 20)))
  top-margin = 1.8\cm
  bottom-margin = 1.8\cm
  two-sided = ##t
  inner-margin = 4.0\cm
  outer-margin = 1.5\cm
  system-system-spacing.minimum-distance = #25
  print-page-number = ##f
}
\header {
  tagline = ##f
  title = ##f
  composer = ##f
  poet = ##f
}
setkey = #(define-music-function (sharps ctx) (integer? string?)
  (let*
    ((tranpitch (if (> sharps 0) (ly:make-pitch 0 4) (ly:make-pitch -1 3)))
     (absharp (abs sharps)))
    (begin
      (while (> absharp 0)
        (begin
	  (assoc-set! pkey ctx (ly:pitch-transpose (assoc-ref pkey ctx) tranpitch))
	  (set! absharp (- absharp 1))))
      #{ \undo \omit Staff.KeySignature \getkey #ctx #})))
getkey = #(define-music-function (idx) (string?)
#{ \key #(assoc-ref pkey idx) \major #}) 
setclef = #(define-music-function (line ctx) (integer? string?)
  (let
    ((newn (+ line (assoc-ref pclef ctx))))
    (begin
      (assoc-set! pclef ctx newn)
      #{ \getclef #ctx #})))
getclef = #(define-music-function (idx) (string?)
  (let
    ((str
       (case (assoc-ref pclef idx)
	 ((1) "soprano")
	 ((2) "mezzosoprano")
	 ((3) "alto")
	 ((4) "tenor")
	 ((5) "baritone")
	 ((-2) "varbaritone")
	 ((-3) "bass")
	 ((-4) "subbass")
	 (else (ly:error "Unrecognised clef number '~a'" (assoc-ref pclef idx))))))
    #{ \clef #str #}))
#(define-markup-command (bar-markup layout props mkp) (markup?)
  (let*
    ((mkpstil (interpret-markup layout props mkp))
     (mkpxext (ly:stencil-extent mkpstil 0))
     (mkpwidth (cdr mkpxext))
     (mkpline (interpret-markup layout props #{\markup \draw-line #`(,mkpwidth . 0) #})))
    (ly:stencil-aligned-to (ly:stencil-combine-at-edge mkpline 1 -1 mkpstil 0.5) Y -1)))
dI = \divisioMaior
dII = \divisioMaxima
teenify = \applyContext #(lambda (ctx)
                          (let ((fs (ly:context-property ctx 'fontSize 0)))
			    (ly:context-set-property! ctx 'fontSize (- fs 3))))
unteenify = \applyContext #(lambda (ctx)
                            (let ((fs (ly:context-property ctx 'fontSize 0)))
			      (ly:context-set-property! ctx 'fontSize (+ fs 3))))
plica = #(define-music-function (note) (ly:music?)
          #{
	    \teenify
	    #note
	    \unteenify
	  #})
ordo = #(define-music-function (ctx vox withkey) (list? ly:music-list? boolean?)
         (let*
	   ((moms (map (lambda (m) (ly:music-length m)) vox))
	    (maxmom (fold (lambda (e p) (if (ly:moment<? p e) e p)) (ly:make-moment 0) moms))
	    (scales (map (lambda (m) (ly:moment-div maxmom m)) moms))
	    (newmus (map (lambda (m s) (ly:music-compress m s)) vox scales))
	    (ctxmus (map (lambda (m c) #{ \new Voice \relative c' { \global
			   \change Staff = #c #(if withkey #{ \getkey #c #}) #m } #}) newmus ctx)))
	   (make-simultaneous-music ctxmus)))
dynskip = #(define-music-function (mus) (ly:music?)
            #{
	      \omit Voice.MultiMeasureRest
	      #(make-music
	        'MultiMeasureRestMusic
		'duration
		(ly:make-duration 0 0 (ly:moment-main (ly:music-length mus))))
	    #})
#(define (add-link url-strg)
   (lambda (grob)
     (let* ((stil (ly:grob-property grob 'stencil)))
       (if (ly:stencil? stil)
	 (begin
	   (let*
	     ((x-ext (ly:stencil-extent stil X))
	      (y-ext (ly:stencil-extent stil Y))
	      (url-expr (list 'url-link url-strg `(quote ,x-ext) `(quote
								    ,y-ext)))
	      (new-stil (ly:stencil-add (ly:make-stencil url-expr x-ext y-ext)
					stil)))
	     (ly:grob-set-property! grob 'stencil new-stil)))
	 #f))))
setl = #(define-music-function (file line chara charb) (string? integer? integer? integer?)
  #{ \once \override NoteHead.after-line-breaking = #(add-link (format #f "textedit://~a:~a:~a:~a" file line chara charb)) #})
#(define (ziplist . xss) (apply map list xss))
div = #(define-music-function (ctx div oids) (string? list? list?)
  (let*
    ((alts (fold (lambda (e prv)
       (if (if (list? (car e)) (not (memq src (car e))))
	 (cons (cadr e) prv)
	 prv)) '() div))
     (longest (cadr
       (assq 'm
         (cadr
	   (car
	     (sort div (lambda (a b)
	       (ly:moment<?
		 (ly:music-length
		   (cadr
		     (assq 'm
		       (cadr b))))
		 (ly:music-length
		   (cadr
		     (assq 'm
		       (cadr a))))))))))))
     (srclist (fold (lambda (e prv)
       (if (if (list? (car e)) (not (memq src (car e))))
	 (cons (car e) prv) prv)) '() div))
     (realmus (cadr
       (assq 'm
	 (cadr
	   (find
	     (lambda (e)
	       (or
		 (eq? (car e) src)
		 (and
		   (pair? (car e))
		   (memq src (car e)))))
	       div)))))
     (realmusmom (ly:music-length realmus))
     (realmuscompratio (ly:moment-div (ly:music-length longest) realmusmom))
     (comprealmus (ly:music-compress realmus realmuscompratio))
     (ossias (fold
       (lambda (e prv)
	 (let*
	   ((thissrcs (caddr e))
	    (thissrc (if (list? thissrcs) (car thissrcs) thissrcs))
	    (kstr (cadr (assq 'k (car e))))
	    (ksplit (string-split kstr #\space))
	    (krejoin (fold-right
	      (lambda (e prv)
		(if (and (pair? prv) (string=? "&" (car prv)))
		  (cons (string-append e " &") (cdr prv))
		  (cons e prv)))
	      '() ksplit))
	    (newk (make-column-markup krejoin))
	    (thismus #{
	      <<
	        {
		  \omit Staff.Clef
		  \override Staff.KeyCancellation.break-visibility = #begin-of-line-visible
		  \set Staff.explicitClefVisibility = #begin-of-line-visible
		  \override Staff.KeySignature.break-visibility = #begin-of-line-visible
		  \startStaff
		  \set Staff.forceClef = ##t
		  \global
		  \shiftOff
		  \voiceNeutralStyle
		  \teeny \getclef #ctx \getkey #ctx #(cadr (assq 'm (car e)))
	        } \new Voice {
		  \shiftOff
		  \voiceNeutralStyle
		  \applyContext #(lambda (ctx)
				   (let
				     ((la (assoc-ref osslastactive (cadr e)))
				      (lsrc (assoc-ref osssrc (cadr e)))
				      (cm (ly:context-current-moment ctx))
				      (srcs (if (list? thissrcs) thissrcs (list thissrcs)))
				      (grob 'TextScript))
				     (if (and (not (ly:moment<? la cm)) ; still at same moment
					      (equal? srcs lsrc)) ; same sources as before
				       (ly:context-pushpop-property ctx grob 'stencil #f)
				       (assoc-set! osssrc (cadr e) srcs))))
		  <>^\markup { \left-align \teeny \override #'(baseline-skip . 1.5) #newk }
		}
	      >>
	      \applyContext #(lambda (ctx) (assoc-set! osslastactive (cadr e) (ly:context-current-moment ctx)))
	      #})
	    (compratio (ly:moment-div (ly:music-length longest) (ly:music-length thismus)))
	    (compmus (ly:music-compress thismus compratio)))
	   (cons (make-music
	     'ContextSpeccedMusic
	     'property-operations '()
	     'context-type 'Staff
	     'context-id (cadr e)
	     'element #{ \startStaff #compmus \stopStaff #}) prv)))
       '() (ziplist alts oids srclist))))
    (make-simultaneous-music (cons comprealmus ossias))))
global = {
  \override Staff.Clef.full-size-change = ##t
  \phrasingSlurUp
  \stemDown
  \hide Stem
  \omit Beam
  \override Stem.length = #0
  \omit Staff.BarLine
  \override Score.SpacingSpanner.packed-spacing = ##t
  \override Score.RehearsalMark.self-alignment-X = #RIGHT
  \override Score.FootnoteItem.annotation-line = ##f
  \override Score.TextSpanner.bound-details.left-broken.text = ##f
  \cadenzaOn
}
