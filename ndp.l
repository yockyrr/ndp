%{
	#include <stdio.h>
	#include <string.h>
	#include "ndp-parser.h"
	#include "ndp-funcs.h"
	#define YYTEXT_DEBUG() ndp_debug(yytext, __FILE__, __LINE__)
	#define YY_USER_ACTION {\
		yylloc.first_line = yylloc.last_line;\
		yylloc.first_column = yylloc.last_column;\
		for(int i=0;yytext[i] != '\0';++i) {\
			if(yytext[i] == '\n') {\
				yylloc.last_line++;\
				yylloc.last_column = 0;\
			} else {\
				yylloc.last_column++;\
			}\
		}\
	}
	int input();
%}
%x DEFS SRCNAME LYRIC NOSRC
%option noinput
%%
"#"[ \t\n]*		{ YYTEXT_DEBUG(); BEGIN DEFS; }
<DEFS>[^ \t\n#]+ 	{ YYTEXT_DEBUG(); yylval.ptr = (void*)strdup(yytext); return STR; }
<DEFS>[ \t\n]+ 		{ YYTEXT_DEBUG(); return DEFSPACE; }
<DEFS>"#" 		{ YYTEXT_DEBUG(); BEGIN INITIAL; }
"{" 			{ YYTEXT_DEBUG(); return OBRACE; }
"}" 			{ YYTEXT_DEBUG(); return CBRACE; }
[a-g] 			{
				YYTEXT_DEBUG();
				yylval.val = (int)*yytext;
				return NOTENAME;
			}
"s" 			{ YYTEXT_DEBUG(); return SPACER; }
"k" 			{ YYTEXT_DEBUG(); return KCHANGE; }
"!" 			{ YYTEXT_DEBUG(); return CCHANGE; }
"p"[a-g]		{
				YYTEXT_DEBUG();
				yylval.val = (int)yytext[1];
				return PLICA;
			}	
"^" 			{ YYTEXT_DEBUG(); return SHARP; }
"_" 			{ YYTEXT_DEBUG(); return FLAT; }
"'" 			{ YYTEXT_DEBUG(); return UP8; }
"," 			{ YYTEXT_DEBUG(); return DOWN8; }
"/" 			{ YYTEXT_DEBUG(); return ALT; }
"[" 			{ YYTEXT_DEBUG(); return OLIG; }
"]" 			{ YYTEXT_DEBUG(); return CLIG; }
"(" 			{ YYTEXT_DEBUG(); return OCUR; }
")" 			{ YYTEXT_DEBUG(); return CCUR; }
"|" 			{ YYTEXT_DEBUG(); return DIV; }
"/\"" 			{ YYTEXT_DEBUG(); BEGIN SRCNAME; return OQUO; }
<SRCNAME>"\""		{ YYTEXT_DEBUG(); BEGIN INITIAL; return CQUO; }
<SRCNAME>[A-Za-z0-9]+ 	{
				YYTEXT_DEBUG();
				yylval.ptr = (void*)strdup(yytext);
				return SRCID;
			}
<SRCNAME>"," 		{ return COMMA; }
"~\"" 			{ YYTEXT_DEBUG(); BEGIN NOSRC; return ONOS; }
<NOSRC>[A-Za-z0-9]+ 	{
				YYTEXT_DEBUG();
				yylval.ptr = (void*)strdup(yytext);
				return SRCID;
			}
<NOSRC>"," 		{ return COMMA; }
<NOSRC>"\"" 		{ YYTEXT_DEBUG(); BEGIN INITIAL; return CNOS; }
"$" 			{ YYTEXT_DEBUG(); BEGIN LYRIC; return LYRDELIM; }
<LYRIC>[^0-9$ \t\n]+ 	{ YYTEXT_DEBUG(); yylval.ptr = (void*)strdup(yytext); return SYLLABLE; }
<LYRIC>[0-9]+ 		{ YYTEXT_DEBUG(); yylval.val = atoi(yytext); return SYLLEN; }
<LYRIC>"$" 		{ YYTEXT_DEBUG(); BEGIN INITIAL; return LYRDELIM; }
<LYRIC>[ \t\n]+ 	{ YYTEXT_DEBUG(); /* do nothing */ }
[ \t\n]+ 		{ YYTEXT_DEBUG(); /* do nothing */ }
. 			{ YYTEXT_DEBUG(); /* do nothing */ }
<<EOF>> 		{ YYTEXT_DEBUG(); return EOFF; }
