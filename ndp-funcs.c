#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <stdbool.h>
#include <stdarg.h>
#include "ndp-funcs.h"
#include "ndp-parser.h"
#include "ily-layout-xxd.c"
void set_outfile(FILE *stream) {
	outfile = stream;
	return;
}
void init_variables() {
	maxv = curv = maxalt = curalt = 0;
	ndp_sourcelist = NULL;
	fullpath = title = poet = composer = NULL;
	pointandclick = false;
	return;
}
void set_title(char *t) {
	assert(t != NULL);
	title = strdup(t);
	return;
}
void set_poet(char *p) {
	assert(p != NULL);
	poet = strdup(p);
	return;
}
void set_composer(char *c) {
	assert(c != NULL);
	composer = strdup(c);
	return;
}
void __ndp_message(YYLTYPE *yylloc, char *type, char *file, int line, char *fmt, ...) {
	va_list ap;
	va_start(ap, fmt);
	fprintf(stderr, "[%s (%s:%d)] ", type, file, line);
	if(yylloc)
		fprintf(stderr, "(@%d:%d-%d:%d) ", yylloc->first_line, yylloc->first_column, yylloc->last_line, yylloc->last_column);
	vfprintf(stderr, fmt, ap);
	fprintf(stderr, "\n");
	return;
}
/* wrapper around malloc to fail with info */
void *__ndp_malloc(size_t c, char *file, int line) {
	void *ret;
	ret = malloc(c);
	if(!ret)
		__ndp_message(NULL, "Error", file, line, "malloc() failed.");
	return ret;
}
/* wrapper around realloc to fail with info */
void *__ndp_realloc(void *ptr, size_t size, char *file, int line) {
	void *ret;
	ret = realloc(ptr, size);
	if(!ret)
		__ndp_message(NULL, "Error", file, line, "realloc() failed.");
	return ret;
}
void emit_base52(int i) {
	assert(i >= 0);
	static char alphabet[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQR\
STUVWXYZ";
	do {
		fputc(alphabet[i % (sizeof alphabet - 1)], outfile);
		i /= sizeof alphabet - 1;
	} while(i);
	return;
}
/* allocate note structure */
note_t make_note(int nn, int acc, int oct, bool plica, YYLTYPE yylloc) {
	note_t ret;
	ret = ndp_malloc(sizeof *ret);
	ret->notename = nn;
	ret->accidental = acc;
	ret->octave = oct;
	ret->plica = plica;
	ret->yylloc = yylloc;
	return ret;
}
/* allocate linked list of notes */
notelist_t make_notelist(note_t n, notelist_t c) {
	assert(n != NULL);
	if(!c) {
		c = ndp_malloc(sizeof *c);
		c->note = n;
		c->next = NULL;
		return c;
	}
	notelist_t new;
	new = ndp_malloc(sizeof *new);
	new->note = n;
	new->next = c;
	return new;
}
/* create wrapper for notelist describing the type of ligature */
ligature_t make_ligature(notelist_t c) {
	assert(c != NULL);
	ligature_t l;
	l = ndp_malloc(sizeof *l);
	l->type = LIGATURE;
	l->noteslen = 1;
	l->notes = ndp_malloc(sizeof *(l->notes));
	l->notes[0] = c;
	return l;
}
/* as above */
ligature_t make_currentes(notelist_t c) {
	assert(c != NULL);
	ligature_t l;
	l = ndp_malloc(sizeof *l);
	l->type = CURRENTES;
	l->noteslen = 1;
	l->notes = ndp_malloc(sizeof *(l->notes));
	l->notes[0] = c;
	return l;
}
/* as above, but this function has two sets of notelists */
ligature_t make_ligcur(notelist_t a, notelist_t b) {
	assert(a != NULL);
	assert(b != NULL);
	ligature_t l;
	l = ndp_malloc(sizeof *l);
	l->type = LIGCUR;
	l->noteslen = 2;
	l->notes = ndp_malloc(sizeof *(l->notes) * 2);
	l->notes[0] = a;
	l->notes[1] = b;
	return l;
}
/* virga only has one note, create notelist for it first */
ligature_t make_virga(note_t n) {
	assert(n != NULL);
	notelist_t nl = make_notelist(n, NULL);
	assert(nl != NULL);
	ligature_t l;
	l = ndp_malloc(sizeof *l);
	l->type = VIRGA;
	l->noteslen = 1;
	l->notes = ndp_malloc(sizeof *(l->notes));
	l->notes[0] = nl;
	return l;
}
/* make a divisio */
ligature_t make_divisio_maior() {
	ligature_t l;
	l = ndp_malloc(sizeof *l);
	l->type = DIVMAJ;
	l->noteslen = 0;
	l->notes = NULL;
	return l;
}
ligature_t make_divisio_maxima() {
	ligature_t l;
	l = ndp_malloc(sizeof *l);
	l->type = DIVMAX;
	l->noteslen = 0;
	l->notes = NULL;
	return l;
}
ligature_t make_keychange(int sharps, int curvidx) {
	ligature_t l;
	l = ndp_malloc(sizeof *l);
	l->type = KEYCHANGE;
	/* bit of a hack to cast int pointer to notelist_t pointer */
	int *hack;
	hack = ndp_malloc(2 * sizeof *hack);
	hack[0] = sharps;
	hack[1] = curvidx;
	l->notes = (notelist_t *)hack;
	l->noteslen = 0;
	return l;
}
ligature_t make_clefchange(int line, int curvidx) {
	ligature_t l;
	l = ndp_malloc(sizeof *l);
	l->type = CLEFCHANGE;
	int *hack;
	hack = ndp_malloc(2 * sizeof *hack);
	hack[0] = line;
	hack[1] = curvidx;
	l->notes = (notelist_t *)hack;
	l->noteslen = 0;
	return l;
}
/* build music from a list of ligatures */
music_t make_music(ligature_t l, music_t m) {
	if(!m) {
		m = ndp_malloc(sizeof *m);
		m->ligature = l;
		m->next = NULL;
		return m;
	}
	music_t new;
	new = ndp_malloc(sizeof *new);
	new->ligature = l;
	new->next = m;
	return new;
}
source_t find_source(char *id) {
	sourcelist_t h;
	h = ndp_sourcelist;
	while(h && strcmp(h->source->id, id))
		h = h->next;
	if(h)
		return h->source;
	ndp_error(NULL, "Could not find source with ID '%s'.", id);
	return NULL;
}
size_t get_source_idx(char *id) {
	sourcelist_t h;
	h = ndp_sourcelist;
	size_t i;
	i = 0;
	while(h && strcmp(h->source->id, id))
		h = h->next;
	if(h)
		return i;
	ndp_error(NULL, "Could not find source with ID '%s'.", id);
	return 0;
}
void make_definition(char *id, char *name) {
	sourcelist_t *h;
	source_t new;
	h = &ndp_sourcelist;
	while(*h)
		h = &((*h)->next);
	*h = ndp_malloc(sizeof **h);
	new = make_source(name, id);
	(*h)->source = new;
	(*h)->next = NULL;
	return;
}
void print_definitions() {
	sourcelist_t h;
	h = ndp_sourcelist;
	while(h) {
		assert(h->source != NULL);
		fprintf(stderr, "Source (ID: '%s', Name: '%s')\n", h->source->id, h->source->name);
		h = h->next;
	}
	return;
}
/* allocate container for arbitrary source */
alt_t make_alt(sourcelist_t s, music_t mus) {
	assert(s != NULL);
	assert(mus != NULL);
	alt_t ret;
	ret = ndp_malloc(sizeof *ret);
	ret->src = s;
	ret->mus = mus;
	return ret;
}
voicelist_t make_nosrc(sourcelist_t s) {
	assert(s != NULL);
	altmus_t a;
	a = ndp_malloc(sizeof *a);
	a->type = NOSRC;
	a->el = (void *)s;
	a->idx = 0;
	a->next = NULL;
	voice_t v;
	v = make_voice(a, NULL);
	voicelist_t vl;
	vl = make_voicelist(v, NULL);
	return vl;
}
source_t sourcelist_source_intersection(sourcelist_t a, sourcelist_t b) {
	assert(a != NULL);
	assert(b != NULL);
	sourcelist_t bcpy;
	while(a) {
		bcpy = b;
		while(bcpy) {
			if(bcpy->source == a->source)
				return a->source;
			bcpy = bcpy->next;
		}
		a = a->next;
	}
	return NULL;
}
bool sourcelist_contains(source_t needle, sourcelist_t haystack) {
	while(haystack) {
		if(haystack->source == needle)
			return true;
		haystack = haystack->next;
	}
	return false;
}
source_t alt_altlist_source_intersection(alt_t a, altlist_t al) {
	assert(a != NULL);
	assert(al != NULL);
	while(al) {
		/* fold through altlist */
		assert(al->alt != NULL);
		source_t intersect;
		intersect = sourcelist_source_intersection(a->src, al->alt->src);
		if(intersect)
			return intersect;
		al = al->next;
	}
	return NULL;
}
/* make a list of alternate versions */
altlist_t make_altlist(alt_t a, altlist_t al, YYLTYPE yylloc) {
	assert(a != NULL);
	if(!al) {
		al = ndp_malloc(sizeof *al);
		al->alt = a;
		al->next = NULL;
		return al;
	}
	source_t intersect;
	intersect = alt_altlist_source_intersection(a, al);
	if(intersect)
		ndp_error(&yylloc, "Source with ID '%s' already described in alternatives.", intersect->id);
	altlist_t new;
	new = ndp_malloc(sizeof *new);
	new->alt = a;
	new->next = al;
	return new;
}
sourcelist_t make_sourcelist(source_t s, sourcelist_t sl) {
	assert(s != NULL);
	if(!sl) {
		sl = ndp_malloc(sizeof *sl);
		sl->source = s;
		sl->next = NULL;
		return sl;
	}
	sourcelist_t new;
	new = ndp_malloc(sizeof *new);
	new->source = s;
	new->next = sl;
	return new;
}
/* make container for mus */
altmus_t make_altmus_from_mus(music_t m, size_t curvidx) {
	assert(m != NULL);
	altmus_t ret;
	ret = ndp_malloc(sizeof *ret);
	ret->type = MUS;
	ret->el = (void *)m;
	ret->idx = curvidx;
	return ret;
}
/* make container for altlist */
altmus_t make_altmus_from_alt(altlist_t al, size_t curvidx) {
	assert(al != NULL);
	altmus_t ret;
	ret = ndp_malloc(sizeof *ret);
	ret->type = ALTLIST;
	ret->el = (void *)al;
	ret->idx = curvidx;
	return ret;
}
/* voice contains both mus and altlist in an altmus container */
voice_t make_voice(altmus_t a, voice_t c) {
	assert(a != NULL);
	if(!c) {
		c = ndp_malloc(sizeof *c);
		c->altmus = a;
		c->next = NULL;
		return c;
	}
	voice_t new;
	new = ndp_malloc(sizeof *new);
	new->altmus = a;
	new->next = c;
	return new;
}
voicelist_t make_voicelist(voice_t v, voicelist_t vl) {
	assert(v != NULL);
	if(!vl) {
		vl = ndp_malloc(sizeof *vl);
		vl->voice = v;
		vl->next = NULL;
		return vl;
	}
	voicelist_t new;
	new = ndp_malloc(sizeof *new);
	new->voice = v;
	new->next = vl;
	return new;
}
/* list of voices */
ordo_t make_ordo(voicelist_t a, ordo_t c) {
	assert(a != NULL);
	if(!c) {
		c = ndp_malloc(sizeof *c);
		c->voicelist = a;
		c->next = NULL;
		return c;
	}
	ordo_t new;
	new = ndp_malloc(sizeof *new);
	new->voicelist = a;
	new->next = c;
	return new;
}
source_t make_source(char *name, char *id) {
	assert(name != NULL);
	assert(id != NULL);
	source_t s;
	s = ndp_malloc(sizeof *s);
	s->name = name;
	s->id = id;
	s->toggled = true;
	s->diffnotes = 0;
	s->diffligs = 0;
	return s;
}
syllable_t make_syllable(char *str, int len) {
	assert(str != NULL);
	assert(len > 0);
	syllable_t s;
	s = ndp_malloc(sizeof *s);
	s->str = str;
	s->len = len;
	return s;
}
lyriclist_t make_lyriclist(syllable_t syl, lyriclist_t ll) {
	assert(syl != NULL);
	if(!ll) {
		ll = ndp_malloc(sizeof *ll);
		ll->syllable = syl;
		ll->next = NULL;
		return ll;
	}
	lyriclist_t new;
	new = ndp_malloc(sizeof *new);
	new->syllable = syl;
	new->next = ll;
	return new;
}
void emit_note(note_t n) {
	if(pointandclick) {
		fputs(ADDURLSTR, outfile);
		fprintf(outfile, "#\"%s\" #%d #%d #%d ", fullpath, n->yylloc.first_line, n->yylloc.first_column, n->yylloc.last_column);
	}
	if(n->plica)
		fputs(PLICASTR, outfile);
	fputc(n->notename, outfile);
	if(n->accidental > 0)
		for(int i = 0; i < n->accidental; ++i)
			fputs(SHARPSTR, outfile);
	else if(n->accidental < 0)
		for(int i = n->accidental; i < 0; ++i)
			fputs(FLATSTR, outfile);
	if(n->octave > 0)
		for(int i = 0; i < n->octave; ++i)
			fputs(UP8STR, outfile);
	else if(n->octave < 0)
		for(int i = n->octave; i < 0; ++i)
			fputs(DOWN8STR, outfile);
	fputs("4 ", outfile);
	return;
}
size_t emit_notelist(notelist_t n) {
	size_t l;
	l = 0;
	while(n) {
		emit_note(n->note);
		++l;
		n = n->next;
	}
	return l;
}
size_t emit_ligature(ligature_t l) {
	size_t len;
	len = 0;
	if(!l) {
		fputs(EMPTYDIVSTR, outfile);
		return 0;
	}
	switch(l->type) {
		case VIRGA:
			assert(l->noteslen == 1);
			len += emit_notelist(l->notes[0]);
			break;

		case LIGATURE:
			assert(l->noteslen == 1);
			fputs(OLIGSTR, outfile);
			len += emit_notelist(l->notes[0]);
			fputs(CLIGSTR, outfile);
			break;

		case CURRENTES:
			assert(l->noteslen == 1);
			emit_note(l->notes[0]->note);
			++len;
			fputs(OCURSTR, outfile);
			len += emit_notelist(l->notes[0]->next);
			fputs(CCURSTR, outfile);
			break;

		case LIGCUR:
			assert(l->noteslen == 2);
			fputs(OLIGSTR, outfile);
			len += emit_notelist(l->notes[0]);
			fputs(OCURSTR, outfile);
			fputs(CLIGSTR, outfile);
			len += emit_notelist(l->notes[1]);
			fputs(CCURSTR, outfile);
			break;

		case DIVMAJ:
			assert(l->noteslen == 0);
			assert(l->notes == NULL);
			fputs(DIVMAJSTR, outfile);
			break;

		case DIVMAX:
			assert(l->noteslen == 0);
			assert(l->notes == NULL);
			fputs(DIVMAXSTR, outfile);
			break;

		case KEYCHANGE:
			assert(l->noteslen == 0);
			fputs(KEYCHANGESTR, outfile);
			fprintf(outfile, "%d #\"staff", ((int *)l->notes)[0]);
			emit_base52(((int *)l->notes)[1]);
			fputs("\" ", outfile);
			break;

		case CLEFCHANGE:
			assert(l->noteslen == 0);
			fputs(CLEFCHANGESTR, outfile);
			fprintf(outfile, "%d #\"staff", ((int *)l->notes)[0]);
			emit_base52(((int *)l->notes)[1]);
			fputs("\" ", outfile);
			break;
	}
	return len;
}
bool source_toggled(source_t s) {
	return s->toggled;
}
size_t emit_music(music_t m, bool countdiff) {
	size_t l, ligs;
	l = ligs = 0;
	while(m) {
		l += emit_ligature(m->ligature);
		m = m->next;
		++ligs;
	}
	if(countdiff) {
		sourcelist_t n = ndp_sourcelist->next;
		while(n) {
			if(!source_toggled(n->source)) {
				/*
				  * ndp_warning(NULL, "Source '%s' not toggled,
				  * add\
				  * ing %lu notes and %lu ligatures (total: %lu
				  * notes, %lu ligatures).",
				  *  n->source->name, l, ligs,
				  *  n->source->diffnotes, n->source->diffligs);
				  */
				n->source->diffnotes += l;
				n->source->diffligs += ligs;
			}
			n = n->next;
		}
	}
	return l;
}
void add_diffs(sourcelist_t src, size_t notes, size_t ligs) {
	assert(src != NULL);
	bool addall = true;
	if(sourcelist_contains(ndp_sourcelist->source, src))
		addall = false;
	while(src) {
		if((addall == false) && (src->source != ndp_sourcelist->source)) {
			src = src->next;
			continue;
		}
		source_t s = src->source;
		s->diffnotes += notes;
		s->diffligs += ligs;
		/*
		  * ndp_warning(NULL, "Source '%s' is described as differing by
		  * %lu\
		  * notes and %lu ligatures (total: %lu notes, %lu ligatures).",
		  * s->name, notes,
		  *  ligs, s->diffnotes, s->diffligs);
		  */
		src = src->next;
	}
	return;
}
size_t count_ligs(music_t mus) {
	size_t ret = 0;
	for(ret = 0; mus;) {
		if(mus->ligature &&
		   (mus->ligature->type != DIVMAJ) &&
		   (mus->ligature->type != DIVMAX) &&
		   (mus->ligature->type != KEYCHANGE) &&
		   (mus->ligature->type != CLEFCHANGE))
			++ret;
		mus = mus->next;
	}
	return ret;
}
/*
  * \div #`(((src) ((k "SRC NAME") (m ,#{ MUSIC #}))) ((src)) ((k "SRC NAME") (m
  * ,#{ MUSIC #})))) \staffalts
  */
size_t emit_altlist(altlist_t al, size_t idx) {
	size_t l, maxl, ligs;
	maxl = l = 0;
	fputs("\\div #\"staff", outfile);
	emit_base52(idx);
	fputs("\" #`(", outfile);
	while(al) {
		alt_t a;
		bool countdiffs = false;
		a = al->alt;
		fputs("((", outfile);
		emit_sourcelist_ids(a->src, outfile);
		fputs(") ((k \"", outfile);
		emit_sourcelist_names(a->src, outfile);
		fputs("\") (m ,#{ ", outfile);
		ligs = count_ligs(a->mus);
		if(sourcelist_contains(ndp_sourcelist->source, a->src))
			countdiffs = true;
		l = emit_music(a->mus, countdiffs);
		add_diffs(a->src, l, ligs);
		if(l > maxl)
			maxl = l;
		fputs("#})))", outfile);
		al = al->next;
	}
	fputs(") \\ossias", outfile);
	emit_base52(idx);
	fputc(' ', outfile);
	return maxl;
}
void emit_sourcelist_ids(sourcelist_t s, FILE *fp) {
	while(s) {
		fputs(s->source->id, fp);
		fputs(" ", fp);
		s = s->next;
	}
	return;
}
void emit_sourcelist_names(sourcelist_t s, FILE *fp) {
	assert(s != NULL);
	fputs(s->source->name, fp);
	s = s->next;
	if(!s)
		return;
	if(!s->next)
		fputs(" ", fp);
	while(s->next) {
		fputs(", ", fp);
		fputs(s->source->name, fp);
		s = s->next;
	}
	fputs("& ", fp);
	fputs(s->source->name, fp);
	return;
}
void free_sourcelist_no_rec(sourcelist_t s) {
	while(s) {
		sourcelist_t p;
		p = s;
		s = s->next;
		free(p);
	}
	return;
}
void toggle_source(source_t s) {
	s->toggled = !(s->toggled);
	return;
}
void emit_nosrc(sourcelist_t s) {
	sourcelist_t h, os;
	h = ndp_sourcelist;
	os = NULL;
	while(s) {
		toggle_source(s->source);
		s = s->next;
	}
	while(h) {
		if(!source_toggled(h->source))
			os = make_sourcelist(h->source, os);
		h = h->next;
	}
	if(os) {
		fputs(NOSRC_BEGIN, outfile);
		emit_sourcelist_names(os, outfile);
		fputs(ONOSRC, outfile);
		free_sourcelist_no_rec(os);
	} else
		fputs(CNOSRC, outfile);
	return;
}
size_t emit_altmus(altmus_t a) {
	assert(a != NULL);
	size_t l, ligs;
	l = 0;
	switch(a->type) {
		case MUS:
			ligs = count_ligs((music_t)a->el);
			ndp_sourcelist->source->diffligs += ligs;
			l = emit_music((music_t)a->el, true);
			ndp_sourcelist->source->diffnotes += l;
			/*
			  * ndp_warning(NULL, "Input %lu notes and %lu ligatures
			  * fo\
			  * r all sources (total: %lu notes, %lu ligatures).",
			  * l, ligs,
			  *  ndp_sourcelist->source->diffnotes,
			  *  ndp_sourcelist->source->diffligs);
			  */
			break;

		case ALTLIST:
			l = emit_altlist((altlist_t)a->el, a->idx);
			break;

		case NOSRC:
			emit_nosrc((sourcelist_t)a->el);
			break;
	}
	return l;
}
size_t emit_voice(voice_t v) {
	size_t l;
	l = 0;
	while(v) {
		l += emit_altmus(v->altmus);
		v = v->next;
	}
	return l;
}
size_t emit_ordo(ordo_t o) {
	size_t maxl, l;
	maxl = l = 0;
	fputs("\\ordo \\vox #(list ", outfile);
	voicelist_t ov;
	ov = o->voicelist;
	while(ov) {
		fputs("#{ ", outfile);
		l = emit_voice(ov->voice);
		if(l > maxl)
			maxl = l;
		fputs("#} ", outfile);
		ov = ov->next;
	}
	fputs(") ##f \\bar \"\"\n", outfile);
	return maxl;
}
size_t *emit_ordines(ordo_t o, size_t *l) {
	*l = 0;
	size_t *r;
	r = NULL;
	while(o) {
		size_t thisl;
		thisl = emit_ordo(o);
		o = o->next;
		if(thisl) {
			++(*l);
			r = ndp_realloc(r, *l * sizeof *r);
			r[*l - 1] = thisl;
		}
	}
	return r;
}
void emit_syllable_skip(int slen, size_t *ordo_l, size_t *ordo_ptr) {
	size_t minus;
	minus = 1;
	for(int i = 0; i < slen; ++i) {
		size_t skip;
		skip = ordo_l[*ordo_ptr] - minus;
		fprintf(outfile, " \\skip 4*%lu ", skip);
		++(*ordo_ptr);
		if(minus)
			minus = 0;
	}
	--(*ordo_ptr);
	return;
}
void emit_large_syllable(syllable_t s, size_t *ordo_l, size_t *ordo_ptr) {
	int alpha;
	assert(s->len > 0);
	fputs("\\markup \\concat { ", outfile);
	alpha = (int)strcspn(s->str, ALPHANUM);
	if(alpha < (int)strlen(s->str))
		fprintf(outfile, "\"%.*s\" ", alpha, s->str);
	fputs("\\fontsize #10 \"", outfile);
	fputc(toupper(s->str[alpha]), outfile);
	fputs("\" \"", outfile);
	fputs(s->str + alpha + 1, outfile);
	fputs("\" }", outfile);
	emit_syllable_skip(s->len, ordo_l, ordo_ptr);
	return;
}
void emit_syllable(syllable_t s, size_t *ordo_l, size_t *ordo_ptr) {
	assert(s->len > 0);
	fprintf(outfile, "\"%s\" ", s->str);
	emit_syllable_skip(s->len, ordo_l, ordo_ptr);
	return;
}
void emit_lyrics(lyriclist_t lyr, size_t *ordo_l, size_t ordo_l_len) {
	assert(ordo_l != NULL);
	assert(ordo_l_len > 0);
	size_t ordo_ptr;
	if(!lyr) {
		fputs("\\lyricmode { }\n", outfile);
		return;
	}
	ordo_ptr = 0;
	fputs("\\lyricmode {\n\\set fontSize = #2\n\\override LyricText.self-a\
lignment-X = #CENTER\n", outfile);
	emit_large_syllable(lyr->syllable, ordo_l, &ordo_ptr);
	++ordo_ptr;
	lyr = lyr->next;
	while(lyr) {
		assert(ordo_ptr < ordo_l_len);
		emit_syllable(lyr->syllable, ordo_l, &ordo_ptr);
		++ordo_ptr;
		lyr = lyr->next;
	}
	assert(ordo_ptr <= ordo_l_len);
	fputs("\n}\n", outfile);
	return;
}
bool stronly(char *s) {
	char *c;
	while((c = strchr(s, '\\')) != NULL) {
		if(c[1] != '\\')
			return false;
		s = c + 1;
	}
	return true;
}
void emit_file(ordo_t o, lyriclist_t lyr) {
	fputs("\n#(define src '", outfile);
	fputs(ndp_sourcelist->source->id, outfile);
	fputs(")\n", outfile);
	fputs("#(define pkey `(", outfile);
	for(size_t i = 0; i < maxv; ++i) {
		fputs("(\"staff", outfile);
		emit_base52(i);
		fputs("\" . ,(ly:make-pitch 0 0)) ", outfile);
	}
	fputs("))\n#(define pclef '(", outfile);
	for(size_t i = 0; i < maxv; ++i) {
		fputs("(\"staff", outfile);
		emit_base52(i);
		fputs("\" . 0) ", outfile);
	}
	fputs("))\n", outfile);
	for(size_t i = 0; i < maxv; ++i) {
		fputs("ossias", outfile);
		emit_base52(i);
		fputs(" = #'(", outfile);
		if(maxalt > 0)
			for(size_t j = 0; j < maxalt - 1; ++j) {
				fputs("\"ossia", outfile);
				emit_base52(i);
				fputs("vs", outfile);
				emit_base52(j);
				fputs("\" ", outfile);
			}
		fputs(")\n", outfile);
	}
	fputs("osssrc = #'(", outfile);
	for(size_t i = 0; i < maxv; ++i)
		if(maxalt > 0)
			for(size_t j = 0; j < maxalt - 1; ++j) {
				fputs(" (\"ossia", outfile);
				emit_base52(i);
				fputs("vs", outfile);
				emit_base52(j);
				fputs("\" . '())", outfile);
			}
	fputs(")\nosslastactive = #`(", outfile);
	for(size_t i = 0; i < maxv; ++i)
		if(maxalt > 0)
			for(size_t j = 0; j < maxalt - 1; ++j) {
				fputs(" (\"ossia", outfile);
				emit_base52(i);
				fputs("vs", outfile);
				emit_base52(j);
				fputs("\" . ,(ly:make-moment 0))", outfile);
			}
	fputs(")\nvox = #'(", outfile);
	for(size_t i = 0; i < maxv; ++i) {
		fputs("\"staff", outfile);
		emit_base52(i);
		fputs("\" ", outfile);
	}
	fputs(")\nmus = {\n", outfile);
	size_t *ordo_lengths, ordo_lengths_len;
	ordo_lengths = emit_ordines(o, &ordo_lengths_len);
	fputs("<>\\stopTextSpan\n}\n\\score{\n\\header {\n", outfile);
	if(title) {
		fputs("  title = ", outfile);
		if(stronly(title))
			fprintf(outfile, "\"%s\"\n", title);
		else
			fprintf(outfile, "%s\n", title);
	}
	fputs("  poet = ", outfile);
	if(poet) {
		if(stronly(poet))
			fprintf(outfile, "\\markup \\column { \\wordwrap { %s }\
\" \" }\n", poet);
		else
			fprintf(outfile, "%s\n", poet);
	} else
		fputs("\\markup \" \"\n", outfile);
	if(composer) {
		fputs("  composer = ", outfile);
		if(stronly(composer))
			fprintf(outfile, "\\markup \\column { \\wordwrap { %s }\
\" \" }\n", composer);
		else
			fprintf(outfile, "%s\n", composer);
	} else
		fputs("\\markup \" \"\n", outfile);
	fputs("}\n<<", outfile);
	for(size_t i = 0; i < maxv; ++i) {
		if(maxalt > 0)
			for(size_t j = maxalt; j > 0; --j) {
				fputs("\\new Staff = \"ossia", outfile);
				emit_base52(i);
				fputs("vs", outfile);
				emit_base52(j - 1);
				fputs("\" \\with { \\magnifyStaff #2/3 \\overri\
de VerticalAxisGroup.remove-first = ##t \\override VerticalAxisGroup.staff-staf\
f-spacing = #'((basic-distance . 0) (minimum-distance . 0) (padding . 1)) \\Rem\
oveEmptyStaves } { \\stopStaff \\omit Staff.KeySignature \\omit Staff.Clef \\gl\
obal \\dynskip \\mus }\n", outfile);
			}
		fputs("\\new Staff = \"staff", outfile);
		emit_base52(i);
		fputs("\" { \\global", outfile);
		if(i)
			fputs(" \\dynskip", outfile);
		fputs(" \\mus }\n", outfile);
	}
	fputs("\\new Lyrics ", outfile);
	emit_lyrics(lyr, ordo_lengths, ordo_lengths_len);
	fputs(">>\n", outfile);
	fwrite(ly_layout_ily, 1, ly_layout_ily_len, outfile);
	fputs("}\n", outfile);
	return;
}
void output_stats() {
	sourcelist_t n = ndp_sourcelist;
	size_t orignotes, origligs;
	orignotes = n->source->diffnotes;
	origligs = n->source->diffligs;
	fprintf(stderr, "Original contained %lu notes, %lu ligatures.\n",
	        orignotes, origligs);
	n = n->next;
	while(n) {
		double similarity;
		similarity = 1.0 - (n->source->diffnotes / (double)orignotes);
		fprintf(stderr, "Source '%s': %lu notes different (%.0lf%% simi\
larity).\n", n->source->name, n->source->diffnotes, similarity * 100);
		similarity = 1.0 - (n->source->diffligs / (double)origligs);
		fprintf(stderr, "Source '%s': %lu ligatures different (%.0lf%% \
similarity).\n", n->source->name, n->source->diffligs, similarity * 100);
		n = n->next;
	}
	return;
}
