%{
	#include <stdio.h>
	#include <stdlib.h>
	#include <unistd.h>
	#include <stdbool.h>
	#include <string.h>
	#include <errno.h>
	#include "ndp-funcs.h"
	#include "ily-beginning-xxd.c"
	#include "usage-xxd.c"
	#include "licence-xxd.c"
	#include "format-xxd.c"
	int yylex();
	void yyerror(char *s);
	FILE *yyin;
#ifdef YYDEBUG
	int yydebug = 1;
#endif
%}
%locations
%token OBRACE CBRACE NOTENAME SHARP FLAT UP8 DOWN8 ALT OLIG CLIG OCUR CCUR
%token SRCID EOFF DIV DEFSPACE STR OQUO CQUO COMMA LYRDELIM SYLLABLE SYLLEN
%token KCHANGE CCHANGE PLICA ONOS CNOS SPACER
%union {
	int val;
	void *ptr;
}
%type <val> up8list down8list NOTENAME ottavalist accidentallist sharplist
%type <val> flatlist SYLLEN PLICA
%type <ptr> altlist alt altmus mus ordolist SRCID ligature note lig cur
%type <ptr> ligcur notelist voice voicelist voiceinner exprs ordo fullalt
%type <ptr> divisio definition definitions STR srcidlist SYLLABLE lyrics
%type <ptr> lyriclist syllable keychange clefchange nosrc
%%
exprs
	: definitions ordolist lyrics EOFF
		{ emit_file($2, $3); $$ = $2; }
	;
lyrics
	: LYRDELIM LYRDELIM
		{ $$ = NULL; }
	| LYRDELIM lyriclist LYRDELIM
		{ $$ = $2; }
	;
lyriclist
	: syllable
		{ $$ = (void*)make_lyriclist($1, NULL); }
	| syllable lyriclist
		{ $$ = (void*)make_lyriclist($1, $2); }
	;
syllable
	: SYLLABLE SYLLEN
		{ $$ = (void*)make_syllable($1, $2); }
	;
definitions
	: definition
	| definition definitions
	;
definition
	: STR DEFSPACE STR DEFSPACE
		{ make_definition((char*)$1, (char*)$3); $$ = $1; }
	;
fullalt
	: altlist ALT
		{
			if(curalt > maxalt)
				maxalt = curalt;
			$$ = $1;
		}
	;
altlist
	: alt
		{ curalt = 1; $$ = (void*)make_altlist($1, NULL, yylloc); }
	| alt altlist
		{ ++curalt; $$ = (void*)make_altlist($1, $2, yylloc); }
	;
alt
	: OQUO srcidlist CQUO mus
		{ $$ = (void*)make_alt((sourcelist_t)$2, $4); }
	;
srcidlist
	: SRCID
		{
			source_t s;
			s = find_source((char*)$1);
			free($1);
			$$ = (void*)make_sourcelist(s, NULL);
		}
	| SRCID COMMA srcidlist
		{
			source_t s;
			s = find_source((char*)$1);
			free($1);
			$$ = (void*)make_sourcelist(s, $3);
		}
	;
nosrc
	: ONOS srcidlist CNOS
		{ $$ = (void*)make_nosrc((sourcelist_t)$2); }
	;
altmus
	: mus
		{ $$ = (void*)make_altmus_from_mus($1, curvidx); }
	| fullalt
		{ $$ = (void*)make_altmus_from_alt($1, curvidx); }
	;
mus
	: ligature
		{ $$ = (void*)make_music($1, NULL); }
	| ligature mus
		{ $$ = (void*)make_music($1, $2); }
	| SPACER
		{ $$ = (void*)make_music(NULL, NULL); }
	;
up8list
	: UP8
		{ $$ = 1; }
	| UP8 up8list
		{ $$ = $2 + 1; }
	;
down8list
	: DOWN8
		{ $$ = -1; }
	| DOWN8 down8list
		{ $$ = $2 - 1; }
	;
ottavalist
	: up8list
	| down8list
		{ $$ = $1; }
	;
note
	: NOTENAME accidentallist ottavalist
		{ $$ = (void*)make_note($1, $2, $3, false, yylloc); }
	| NOTENAME accidentallist
		{ $$ = (void*)make_note($1, $2, 0, false, yylloc); }
	| NOTENAME ottavalist
		{ $$ = (void*)make_note($1, 0, $2, false, yylloc); }
	| NOTENAME
		{ $$ = (void*)make_note($1, 0, 0, false, yylloc); }
	| PLICA accidentallist ottavalist
		{ $$ = (void*)make_note($1, $2, $3, true, yylloc); }
	| PLICA accidentallist
		{ $$ = (void*)make_note($1, $2, 0, true, yylloc); }
	| PLICA ottavalist
		{ $$ = (void*)make_note($1, 0, $2, true, yylloc); }
	| PLICA
		{ $$ = (void*)make_note($1, 0, 0, true, yylloc); }
	;	
keychange
	: KCHANGE ottavalist
		{ $$ = (void*)make_keychange($2, (int)curvidx); }
	;
clefchange
	: CCHANGE ottavalist
		{ $$ = (void*)make_clefchange($2, (int)curvidx); }
	;
accidentallist
	: sharplist
	| flatlist
		{ $$ = $1; }
	;
sharplist
	: SHARP
		{ $$ = 1; }
	| SHARP sharplist
		{ $$ = $2 + 1; }
	;
flatlist
	: FLAT
		{ $$ = -1; }
	| FLAT flatlist
		{ $$ = $2 - 1; }
	;
ligature
	: lig
	| cur
	| ligcur
	| divisio
	| keychange
	| clefchange
		{ $$ = $1; }
	| note
		{ $$ = (void*)make_virga((note_t)$1); }
	;
divisio
	: DIV
		{ $$ = (void*)make_divisio_maior(); }
	| DIV DIV
		{ $$ = (void*)make_divisio_maxima(); }
	;
lig
	: OLIG notelist CLIG
		{ $$ = (void*)make_ligature((notelist_t)$2); }
	;
cur
	: OCUR notelist CCUR
		{ $$ = (void*)make_currentes((notelist_t)$2); }
	;
ligcur
	: OLIG notelist OCUR CLIG notelist CCUR
		{ $$ = (void*)make_ligcur((notelist_t)$2, (notelist_t)$5); }
	;
notelist
	: note
		{ $$ = (void*)make_notelist($1, NULL); }
	| note notelist
		{ $$ = (void*)make_notelist($1, (notelist_t)$2); }
	;
voice
	: OBRACE voiceinner CBRACE
		{
			++curvidx;
			++curv;
			if(curv > maxv)
				maxv = curv;
			$$ = $2;
		}
	;
voiceinner
	: altmus
		{ $$ = (void*)make_voice($1, NULL); }
	| altmus voiceinner
		{ $$ = (void*)make_voice($1, $2); }
	;	
voicelist
	: voice
		{ $$ = (void*)make_voicelist($1, NULL); }
	| voice voicelist
		{ $$ = (void*)make_voicelist($1, $2); }
	;
ordo
	: OBRACE voicelist CBRACE
		{ curv = 0; curvidx = 0; $$ = $2; }
	;
ordolist
	: ordo
		{ $$ = (void*)make_ordo($1, NULL); }
	| nosrc
		{ $$ = (void*)make_ordo($1, NULL); }
	| ordo ordolist
		{ $$ = (void*)make_ordo($1, $2); }
	| nosrc ordolist
		{ $$ = (void*)make_ordo($1, $2); }
	;
%%
int main(int argc, char **argv) {
	FILE *outputfile;
	FILE **inputfiles = NULL;
	char **titles, **poets, **composers;
	size_t i, ninputs, ntitles, npoets, ncomposers;
	ninputs = ntitles = npoets = ncomposers = 0;
	titles = NULL;
	poets = NULL;
	composers = NULL;
	outputfile = stdout;
	int c;
	yyin = stdin;
	while((c = getopt(argc, argv, "t:p:c:i:o:k?hlH")) != -1)
		switch(c) {
			case 't':
				++ntitles;
				titles = ndp_realloc(titles, ntitles * sizeof
*titles);
				titles[ntitles-1] = strdup(optarg);
				break;
			case 'p':
				++npoets;
				poets = ndp_realloc(poets, npoets * sizeof
*poets);
				poets[npoets-1] = strdup(optarg);
				break;
			case 'c':
				++ncomposers;
				composers = ndp_realloc(composers, ncomposers *
sizeof *composers);
				composers[ncomposers-1] = strdup(optarg);
				break;
			case 'i':
				;
				long pathmax;
				FILE *tmp;
				pathmax = pathconf(optarg, _PC_PATH_MAX);								
				fullpath = ndp_malloc(pathmax+1);
				fullpath = realpath(optarg, fullpath);
				if(!fullpath) {
					fprintf(stderr, "Error: %s\n",
strerror(errno));
					exit(1);
				}
				tmp = NULL;
				++ninputs;
				inputfiles = ndp_realloc(inputfiles,
ninputs * sizeof *inputfiles);
				
				tmp = fopen(fullpath, "r");
				if(!tmp) {
					fprintf(stderr, "Error: %s\n",
strerror(errno));
					exit(1);
				}
				inputfiles[ninputs-1] = tmp;
				break;
			case 'o':
				outputfile = fopen(optarg, "w");
				if(!outputfile) {
					fprintf(stderr, "Error: %s\n",
strerror(errno));
					exit(1);
				}
				break;
			case 'k':
				pointandclick = true;
				break;
			case 'h':
			case '?':
				;
				char *buf;
				buf = ndp_malloc(usage_txt_len + 1);
				memcpy(buf, usage_txt, usage_txt_len);
				buf[usage_txt_len] = '\0';
				printf(buf, argv[0]);
				free(buf);
				return 0;
			case 'l':
				fwrite(LICENCE_txt, 1, LICENCE_txt_len, stdout);
				return 0;
			case 'H':
				fwrite(format_txt, 1, format_txt_len, stdout);
				return 0;
			default:
				fprintf(stderr, "Unknown option: -%c.\n", c);
				fprintf(stderr, "Type %s -h for usage.\n",
argv[0]);
				return EXIT_FAILURE;
		}
	set_outfile(outputfile);
	if(!((ninputs == ntitles) && (ntitles == npoets) && (npoets ==
ncomposers))) {
		fprintf(stderr, "Error: number of titles (%lu) must equal numbe\
r of composers (%lu) must equal number of poets (%lu) must equal number of inpu\
ts (%lu).\n", ntitles, ncomposers, npoets, ninputs);
		return EXIT_FAILURE;
	}
	fwrite(ly_beginning_ily, 1, ly_beginning_ily_len, outfile);
	if(ninputs)
		for(i = 0;i < ninputs; ++i) {	
			init_variables();
			set_title(titles[i]);
			set_poet(poets[i]);
			set_composer(composers[i]);
			yyin = inputfiles[i];
			yyparse();
			fprintf(stderr, "Max voices was %lu\n", maxv);
			output_stats();
			fclose(yyin);
		}
	else {
		init_variables();
		yyparse();
		fprintf(stderr, "Max voices was %lu\n", maxv);
		output_stats();
		fclose(yyin);
	}
	fclose(outputfile);
	return EXIT_SUCCESS;
}
void yyerror(char *s) {
	fprintf(stderr, "Error %d:%d-%d:%d: %s\n", yylloc.first_line,
yylloc.first_column, yylloc.last_line, yylloc.last_column, s);
}
